# Scraping Login forms for OAUTH
This repository shortly demonstrates how scraping a website for login pages 
and the existence of OAUTH flows could be approached.

## Technologies used  
**Scrapy**  
https://docs.scrapy.org/en/latest/  
**Scrapy-Splash**  
https://github.com/scrapy-plugins/scrapy-splash  
**BeautifulSoup**   
https://www.crummy.com/software/BeautifulSoup/bs4/doc/  

## Starting the project
Run `pip install -r requirements.txt`  (or make a venv and then install )  
Execute in the main directory `python login/main.py`  
OR use scrapy in the scrapy project directory (./login)  
`scrapy crawl login-oauth -O output.json`

## Steps to reproduce
Create a new directory  
Use pip to install scrapy, scrapy-splash, bs4  
Either copy the files in this repository and adjust or:  
`scrapy startproject login`  
`cd login`  
`scrapy genspider login_oauth stackoverflow.com`  
Change settings according to scrapy-splash docs  
Create main.py with similar code into the scrapy project directory  
Change spider according to needs, see current spider for annotated example

