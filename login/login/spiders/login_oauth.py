import json
import re

import scrapy
import urllib.parse
from scrapy_splash import SplashRequest
from bs4 import BeautifulSoup


class LoginOauthSpider(scrapy.Spider):
    # Can invoke crawler programmatically with example-scraping-login/login/main.py
    # Can also use terminal command: scrapy crawl login-oauth -O result.json
    # Outputs the yields in the parse function to result.json
    name = 'login-oauth'

    # This gets called on executing the spider
    # Multiple URL's can be defined, or for example be passed into this class with an __init__ method
    # We then use Splash to emulate a browser, so that dynamic content that is injected with javascript
    #   is rendered.
    # To configure Splash, go to: https://github.com/scrapy-plugins/scrapy-splash
    # Make sure to have the docker image running and the project settings adjusted
    def start_requests(self):
        urls = [
            'https://stackoverflow.com/'
        ]

        for url in urls:
            yield SplashRequest(url=url, callback=self.parse)

    # This method is called for every URL request
    # We use Beautifulsoup to parse the response body since I'm familiar with it,
    #   however, scrapy also offers xpath or css selectors to traverse the html document
    # If a login page link is detected it follows the link with a splash request and invokes the discover_auth method
    def parse(self, response):
        soup = BeautifulSoup(response.body, "html.parser")
        login_page_link = self.find_login_link(soup)
        if login_page_link is not None:
            # yield response.follow(login_page_link, self.discover_oauth)
            yield SplashRequest(response.urljoin(login_page_link), self.discover_oauth)
        else:
            yield {"url": response.url, "login_page": "Unavailable", "OAUTH": "Unavailable"}

    # Here we construct a new soup for the login page
    # We can call methods here to search for specific OAUTH flows
    # For this POC we search for Google's OAUTH
    # Lastly we write the result to a file and return the result.
    def discover_oauth(self, response):
        soup = BeautifulSoup(response.body, "html.parser")
        google_oauth = self.discover_oauth_google(soup)

        result = {"url": response.url, "login_page": "Available", "OAUTH": {"Google_OAUTH": google_oauth}}
        self.save_output(response=response, result=result)
        return result

    def discover_oauth_google(self, soup):
        # Referencing branding guideline oauth google:
        # https://developers.google.com/identity/branding-guidelines
        # There still might be variations or different languages used
        # It's possible to scan for a broader range of matches
        regex = re.compile('.*log in with google*.')

        # Use a html parser to find all link or button elements
        # BS4 was used here because I'm familiar with it
        # Scrapy also supports xpath or css selectors
        all_links = soup.find_all("a")
        all_buttons = soup.find_all("button")
        found = False

        # Check if one of the elements matches with the regex defined above
        for link in all_links:
            found = self.compare_text_from_el(link, regex)
            if found:
                break
        if not found:
            for button in all_buttons:
                found = self.compare_text_from_el(button, regex)
                if found:
                    break
        return found

    def compare_text_from_el(self, element, regex):
        text = element.get_text().lower()
        if re.search(regex, text):
            return True
        return False

    def find_login_link(self, soup):
        # Create regex to try to find login page
        # Should look for different keywords to guarantee matches on all kinds of websites
        regex = re.compile('.*login*.')
        link_el = soup.find("a", href=regex)
        link = None
        if link_el is not None:
            link = link_el["href"]
        return link

    def save_output(self, response, result):
        domain = urllib.parse.urlparse(response.url).netloc
        filename = f'website-{domain}.txt'
        with open(filename, 'w') as f:
            f.write(json.dumps(result))
        self.log(f'Saved result for: {domain}')
